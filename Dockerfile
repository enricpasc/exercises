FROM frolvlad/alpine-miniconda3

RUN pip install pandas

WORKDIR /app

COPY . /app

ENTRYPOINT [ "python3", "parser.py" ]

