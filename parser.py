#%% import libraries
from pathlib import Path
import argparse
from re import A
import sys
from weakref import finalize
import pandas as pd
import json

#Transformation functions. In a production environment, they would be located in a different Python file, so they could be used by other applications.

def birthday_to_age(bookings, fields):
    now = pd.Timestamp('now')
    for field in fields:
        col=field['field']
        new_col=field['new_field']
        bookings[col] = pd.to_datetime(bookings[col]) 
        bookings[col] = bookings[col].where(bookings[col] < now, bookings[col].fillna(0)) 
        bookings[new_col] = (now - bookings[col]).astype('<m8[Y]')


def fill_empty_values(bookings, fields):
    for field in fields:
        col=field['field']
        val=field['value']
        if col=='km':
            if val=='mean':
                bookings[col].fillna((bookings[col].mean()), inplace=True)
            if val=='median':
                bookings[col].fillna((bookings[col].median()), inplace=True)
            if val=='mode':
                mode = bookings[col].mode()
                bookings[col].fillna(mode, inplace=True)
        if col=='user_name':
            bookings[col].fillna(value=val, inplace=True)


# Initiate the parser
parser = argparse.ArgumentParser(description='Import JSON or CSV file with config')

parser.add_argument('--config', type=Path,
                    help='An optional integer argument')
args = parser.parse_args()

print(args.config)

if args.config is None:
     sys.exit('error the config file path must be provided')

#check if file exists
try:
    config_file = open(args.config)
    
except FileNotFoundError:
    sys.exit('the config file does not exists')

print(config_file)

#obtain content from config file and parse to JSON
try:
    config_JSON = json.loads(config_file.read())
except TypeError:
    sys.exit('the config file is not a valid JSON')
source=config_JSON['source']
file_path=source['path']+source['dataset']+'.'+source['format']
print(file_path)

#open data file path and normalise according to file format CSV or JSON lines. 
#A pandas dataframe is created.
try:
    if source['format']=='csv':
        bookings=pd.read_csv(file_path, sep=',')
    elif source['format']=='json':
        bookings=pd.read_json(file_path)
    else: 
        sys.exit('the file format is not a csv or json')
except FileNotFoundError:
     sys.exit('No such file or directory')
print(bookings)

#Transformations:

for transform_json in config_JSON['transforms']:
    transform=transform_json['transform']
    fields=transform_json['fields']
    if transform=='birthdate_to_age':
        birthday_to_age(bookings, fields)
    if transform=='hot_encoding':
        bookings=pd.get_dummies(bookings, prefix=fields, columns=fields, dummy_na=True) #the Pandas library get dummies transforms categorical columns into binary data
    if transform=='fill_empty_values':
        fill_empty_values(bookings, fields)

print(bookings)

#save dataframe into destination with desired format (CSV or JSON)
destination=config_JSON['sink']
file_path_destination=destination['path']+destination['dataset']+'.'+destination['format']

if destination['format']=='csv':
        bookings.to_csv(file_path_destination,index=False)
elif destination['format']=='json':
        bookings.to_json(file_path_destination,orient='records', lines=True)
else: 
        bookings.sys.exit('the file format is not a csv or json')


